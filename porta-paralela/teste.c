/* $Id$ */
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define BIT0  0x01  // D0
#define BIT1  0x02  // D1
#define BIT2  0x04  // D2
#define BIT3  0x08  // D3
#define BIT4  0x10  // D4
#define BIT5  0x20  // D5
#define BIT6  0x40  // D6
#define BIT7  0x80  // D7


#define distancia_sens 0.1

typedef short (_stdcall *inpfuncPtr)(short portaddr);
typedef void (_stdcall *oupfuncPtr)(short portaddr, short datum);

HINSTANCE hLib; 

inpfuncPtr inp32fp;
oupfuncPtr oup32fp;


short Inp32 (short portaddr)
{
	return (inp32fp)(portaddr);
}

void Out32 (short portaddr, short datum)
{
	(oup32fp)(portaddr,datum);
}


int main(void)
{
	unsigned char Byte; //Para armazenar o byte recebido da Porta Paralela
	int marcou;
	double gravidade = 9.8,
	    trabalho=0.0,
	    aceleracao_media,
	    aceleracao1=0.0, aceleracao2=0.0, aceleracao3=0.0,
		velocidade=0.0, velocidade2 = 0.0, velocidade3 = 0.0,
	    distancia_compressao=0.0, distancia = 0.0,
	    velocidadem = 0.0,
	    massa = 0.0,
	    h = 0.0,
	    atrit_pista = 0.0,
	    energia = 0.0, energia2 = 0.0,
	    acele = 0.0,
	    coef_atrit_pista = 0.0,
	    distancia_pista = 0,
	    tempo=0.0, tempo1=0.0, tempo2=0.0,
	    T = 0.0, F = 0.0, U = 0.0, K = 0.0,
	    Ener_cinetica = 0.0,
	    k_mola = 0.0;
	DWORD antes, agora;

	hLib = LoadLibrary("inpout32.dll");
	if (hLib != NULL) {
		printf("DLL carregada com sucesso !!! \n");
	}
	else {
		printf("Problemas ao carregar a DLL !!! \n");
		exit(1);
	}

	inp32fp = (inpfuncPtr) GetProcAddress(hLib, "Inp32");
	if (inp32fp != NULL) {
		printf("Funcao 'Inp32' encontrada ! \n");
	}
	else {
		printf("Problemas para obter o endere�o da funcao 'Inp32'... \n");
		exit(1);
	}

	oup32fp = (oupfuncPtr) GetProcAddress(hLib, "Out32");
	if (oup32fp != NULL) {
		printf("Funcao 'Out32' encontrada ! \n");
	}
	else {
		printf("Problemas para obter o endere�o da funcao 'Out32'... \n");
		exit(1);
	}

	double diff;
	marcou = 0;

	Out32(0x37a,0x23); /* habilita porta LPT1 como entrada */ 
	while(1) {
		Byte = (unsigned char)Inp32(0x378); /* Ler um byte da Porta Paralela */

		if((Byte & BIT1) == 0) {
			if (!marcou) {
				antes = GetTickCount();
				printf("comecei a marcar\n");
				marcou = !marcou;
				
			}
			else {
				agora = GetTickCount();
				diff = ((double)agora-(double)antes) / 1000.0;
				printf("santa diferenca: %.4lf (%d .. %d)\n", diff, agora, antes);
				marcou = !marcou;
				tempo=diff;
				
			}
			/*Calculo da velocidade 1 */
			velocidade=distancia/diff;
		} 
		else if((Byte & BIT2) == 0) {                     
			if (!marcou) {
				antes = GetTickCount();
				printf("comecei a marcar\n");
				marcou = !marcou;
			}
			else {
				agora = GetTickCount();
				diff = ((double)agora-(double)antes) / 1000.0;
				printf("santa difereca: %.4lf (%d .. %d)\n", diff, agora, antes);
				marcou = !marcou ;
				tempo1=diff;
			}
			/*Calculo da velocidade 2 */
			velocidade2=distancia/diff;
		}     
		else if((Byte & BIT3) == 0) {                     
			if (!marcou) {
					antes = GetTickCount();
					printf("comecei a marcar\n");
					marcou = !marcou;
			}
			else {
				agora = GetTickCount();
				diff = ((double)agora-(double)antes) / 1000.0;
				printf("santa difereca: %.4lf (%d .. %d)\n", diff, agora, antes);
				marcou = !marcou;
				tempo2=diff;
			}
			/*Calculo da velocidade 3*/
			velocidade3=distancia/diff;
		}
		else if((Byte & BIT4) == 0) {
			printf("INATIVO \n");
		}
		else if((Byte & BIT5) == 0) {
			printf("INATIVO \n");
			
		}
		else if((Byte & BIT6) == 0) {
			printf("INATIVO \n");
		}
		else if((Byte & BIT7) == 0) {
			printf("INATIVO \n");
		}
		Sleep(300L);
	} /* while !kbhit() */

	/*Calculo da energia Pontencial */
	U = massa*gravidade*h;

	/*Calculo da energia cinetica no 1 sensor*/

	Ener_cinetica = ((massa*(velocidade)*(velocidade))/2.0);

	/*Caculo da velocidade media*/
	
	velocidadem = ((velocidade+velocidade2+velocidade3)/3.0);

	/*Calculo do atrito da pista*/
	
	energia = ((massa*(velocidade*velocidade))/2.0);
	energia2 = ((massa*(velocidade2*velocidade2))/2.0);
	atrit_pista = ((energia2-energia)/distancia_sens);
	coef_atrit_pista = (atrit_pista/(massa*gravidade));

	/*Calculo da aceleracao media*/
	    
	aceleracao1=velocidade/tempo;
	aceleracao2=velocidade2/tempo1;
	aceleracao3=velocidade3/tempo2;
	aceleracao_media=((aceleracao1+aceleracao2+aceleracao3)/3.0);
	
	/* Calculo do trabalho do corpo */
	F = massa*aceleracao_media;
	T = F*distancia_pista; 

	/* Calculo energia cinetica do corpo */
	K=(massa*((velocidade3)*(velocidade3))/2.0);

	/* Calculo do K da mola */

	trabalho=Ener_cinetica;
	k_mola=trabalho/(0.5*(distancia_compressao*distancia_compressao));

	printf("A constante da mola �: %f\n", k_mola);
	printf("Energia potencial da mola: %f\n",trabalho);
	printf("Energia potencial do corpo: %f\n", U);
	printf("Energia cin�tica: %f\n", K);
	printf("Trabalho do corpo: %f\n", T);
	printf("Coeficiente de atrito da pista: %f\n", coef_atrit_pista);
	printf("Velocidade media: %f\n", velocidadem);
	printf("Acelera�ao media: %f\n",aceleracao_media);

	return 0;
}

