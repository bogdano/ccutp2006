#ifndef inc_paralela_win32
#define inc_paralela_win32

#include "paralela.h"

#include <windows.h>

typedef short (_stdcall *inpfuncPtr)(short portaddr);
typedef void (_stdcall *oupfuncPtr)(short portaddr, unsigned char datum);

#endif /* #ifndef inc_paralela_win32 */
