#ifndef inc_paralela_h
#define inc_paralela_h


int inicializa_leitor_paralela(void);

#ifdef WIN32
#include "paralela_win32.h"

unsigned char inb(short);
void outb(unsigned char, short);

#else
#include "paralela_posix.h"
#endif


#endif /* #ifndef inc_paralela_h */

