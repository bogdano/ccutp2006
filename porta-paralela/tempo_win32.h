#ifndef inc_tempo_win32_h
#define inc_tempo_h

#include <windows.h>

#define usleep(t)	Sleep(((unsigned long)t)/1000)

int gettimeofday(struct timeval*, struct timezone*);

#endif