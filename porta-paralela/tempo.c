#include "tempo.h"

double gettimedelta(struct timeval *tv1, struct timeval *tv2)
{
	double delta;

	delta = ((tv1->tv_usec - tv2->tv_usec) + 
	         (tv1->tv_sec - tv2->tv_sec) * 10e5);
	return delta;
}
