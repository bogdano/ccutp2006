#include "tempo_win32.h"


int gettimeofday(struct timeval *tv, struct timezone *tz) {
	/* obtido de: svn://194.231.39.21/tnn/trunk/os/win32/win32.c */
	SYSTEMTIME systm;
	FILETIME   ftm;
	LONGLONG   ll;

	GetSystemTime(&systm);
	if (!SystemTimeToFileTime(&systm,&ftm))
		return -1;

	ll = (LONGLONG) ftm.dwHighDateTime;
	ll <<= 32;
	ll |= (LONGLONG) ftm.dwLowDateTime;
	ll -= 116444736000000000;
	ll /= 10000000;
	tv->tv_sec = (long) ll;
	tv->tv_usec = (long) systm.wMilliseconds * 1000;
	return 0;
}
