#include "paralela_win32.h"

inpfuncPtr inp32fp;
oupfuncPtr oup32fp;

unsigned char inb(short portaddr)
{
	return (unsigned char)(inp32fp)(portaddr);
}

void outb(unsigned data, short portaddr)
{
	(oup32fp)(portaddr, data);
}

int inicializa_leitor_paralela()
{
	HINSTANCE hLib;

	hLib = LoadLibrary("inpout32.dll");
	if (!hLib){
		return -1;
	}

	inp32fp = (inpfuncPtr) GetProcAddress(hLib, "Inp32");
	if (!inp32fp){
		return -2;
	}

	oup32fp = (oupfuncPtr) GetProcAddress(hLib, "Out32");
	if (!oup32fp) {
		return -3;
	}

	return 0;
}
