/* $Id$ */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define DEBUG
#undef USAR_PARALELA

#include "paralela.h"
#include "tempo.h"


#define distancia_sens	0.31 


void bits(unsigned char byte, char *dest)
{
	int i = 0;
	
	for (i = 0; i < 8; i++)
		dest[i] = (byte & (1 << i)) ? '1' : '0';
	dest[i] = 0;
}

int pegar_tempo_rodas(unsigned char bit, struct timeval *t1,
                      struct timeval *t2)
{
	unsigned char lido;
	int marcar_roda = 1;
	int primeira_roda = 1;
	char representacao[9];
	unsigned char comparacao;

	comparacao = 1 << bit;

	while (1) {
		lido = ~inb(0x378);

#ifdef DEBUG
		bits(lido, representacao);
		printf("\rvalor: %s [%03u] comp: %03u", 
		       representacao, lido, comparacao);
#endif
		
		if (lido & comparacao) {
			if (marcar_roda) {
				if(primeira_roda) {
#ifdef DEBUG
					printf("\nmarcando\n");
#endif
					gettimeofday(t1, NULL);
					primeira_roda = 0;
				}
				else {
					gettimeofday(t2, NULL);
					return 1;
				}
			}
			marcar_roda = 0;
		}
		else {
			marcar_roda = 1;
		}
		usleep(1000);
	}
}

double tempo_entre_rodas(unsigned char bit)
{
#ifdef USAR_PARALELA
	struct timeval tv1, tv2;

	pegar_tempo_rodas(bit, &tv1, &tv2);
	return gettimedelta(&tv2, &tv1);
#else
	double tempo;
	printf("Informe o tempo para o sensor %u: ", bit);
	scanf("%lf", &tempo);
	return tempo;
#endif
}

int main(void)
{
	double gravidade = 0.0,
	    trabalho=0.0,
	    aceleracao_media,
	    aceleracao1=0.0, aceleracao2=0.0, aceleracao3=0.0,
	    velocidade=0.0, velocidade2 = 0.0, velocidade3 = 0.0,
	    distancia_compressao=0.0, distancia_rodas = 0.0,
	    velocidadem = 0.0,
	    massa = 0.0,
	    h = 0.0,
	    atrit_pista = 0.0,
	    energia = 0.0, energia2 = 0.0,
	    coef_atrit_pista = 0.0,
	    distancia_pista = 0,
	    tempo=0.0, tempo1=0.0, tempo2=0.0,
	    T = 0.0, F = 0.0, U = 0.0, K = 0.0,
	    Ener_cinetica = 0.0,
	    k_mola = 0.0;

	FILE *fsaida;
	
	/* apenas para debug */
#ifdef DEBUG
	struct timeval t1, t2;
	int i = 0;
#endif

#ifdef USAR_PARALELA
	if(inicializa_leitor_paralela() < 0) {
		fprintf(stderr, "Nao foi possivel inicializar o leitor "
		                "da porta!\n");
		return 1;
	}
	outb(0x23, 0x37a); /* sinal m�gico para habilitar leitura da 0x378 */
	outb(0xff, 0x378);

#ifdef DEBUG
	while(1) {
		for (i = 0; i < 2; i++) {
			pegar_tempo_rodas(5 + i, &t1, &t2);
			printf("diff: %.4lfs\n", 
			       gettimedelta(&t2, &t1) / 10e5);
		}
		
	}
#endif
#endif

	printf("Informe gravidade:");
	scanf("%lf",&gravidade);
	printf("Informe distancia do eixo:");
	scanf("%lf",&distancia_rodas);
	printf("Informe comprimento da pista:");
	scanf("%lf",&distancia_pista);
	printf("Informe altura da pista:");
	scanf("%lf",&h);
	printf("Informe massa do objeto:");
	scanf("%lf",&massa);
	printf("Informe distancia de compressao da mola:");
	scanf("%lf",&distancia_compressao);

	tempo = tempo_entre_rodas(1);
	tempo1 = tempo_entre_rodas(2);
	tempo2 = tempo_entre_rodas(3);

	velocidade = distancia_rodas / tempo;
	velocidade2 = distancia_rodas / tempo1;
	velocidade3 = distancia_rodas / tempo2;

	/*Calculo da energia Pontencial */
	U = massa*gravidade*h;

	/*Calculo da energia cinetica no 1 sensor*/

	Ener_cinetica = ((massa*(velocidade)*(velocidade))/2.0);

	/*Caculo da velocidade media*/
	
	velocidadem = ((velocidade+velocidade2+velocidade3)/3.0);

	/*Calculo do atrito da pista*/
	
	energia = ((massa*(velocidade*velocidade))/2.0);
	energia2 = ((massa*(velocidade2*velocidade2))/2.0);
	atrit_pista = ((energia2-energia)/distancia_sens);
	coef_atrit_pista = -(atrit_pista/(massa*gravidade));

	/*Calculo da aceleracao media*/
	    
	aceleracao1=velocidade/tempo;
	aceleracao2=velocidade2/tempo1;
	aceleracao3=velocidade3/tempo2;
	aceleracao_media=((aceleracao1+aceleracao2+aceleracao3)/3.0);
	
	/* Calculo do trabalho do corpo */
	F = massa*aceleracao_media;
	T = F*distancia_pista; 

	/* Calculo energia cinetica do corpo */
	K=(massa*((velocidade3)*(velocidade3))/2.0);

	/* Calculo do K da mola */

	trabalho=Ener_cinetica;
	k_mola=trabalho/(0.5*(distancia_compressao*distancia_compressao));

	printf("A constante da mola e: %lf N/m \n", k_mola);
	printf("Energia potencial da mola: %lf J \n",trabalho);
	printf("Energia potencial do corpo na descida: %lf J \n", U);
	printf("Energia cinetica do corpo: %lf J \n", K);
	printf("Trabalho do corpo: %lf J \n", T);
	printf("Coeficiente de atrito da pista: %lf\n", coef_atrit_pista);
	printf("Velocidade media: %lf m/s \n", velocidadem);
	printf("Aceleracao media: %lf m/s^2 \n", aceleracao_media);

	fsaida = fopen("dados-pcarrinho.txt", "a+");
	if (!fsaida) {
		perror("nao foi possivel abrir arquivo de saida");
		return 1;
	}

	fprintf(fsaida, "gravidade: %lf\n", gravidade);
	fprintf(fsaida, "distancia-eixo: %lf\n", distancia_rodas);
	fprintf(fsaida, "comprimento-pista: %lf\n", distancia_pista);
	fprintf(fsaida, "altura-pista: %lf\n", h);
	fprintf(fsaida, "massa: %lf\n", massa);
	fprintf(fsaida, "distancia-compressao: %lf\n", distancia_compressao);
	fprintf(fsaida, "tempo0: %lf\n", tempo);
	fprintf(fsaida, "tempo1: %lf\n", tempo1);
	fprintf(fsaida, "tempo2: %lf\n", tempo2);
	fprintf(fsaida, "constante-mola: %lf\n", k_mola);
	fprintf(fsaida, "energia-potencial-mola: %lf\n", trabalho);
	fprintf(fsaida, "energia-potencial-corpo: %lf\n", U);
	fprintf(fsaida, "energia-cinetica-corpo: %lf\n", K);
	fprintf(fsaida, "trabalho-corpo: %lf\n", T);
	fprintf(fsaida, "coef-atrito-pista: %lf\n", coef_atrit_pista);
	fprintf(fsaida, "velocidade-media: %lf\n", velocidadem);
	fprintf(fsaida, "aceleracao-media: %lf\n", aceleracao_media);
	fprintf(fsaida, "\n\n"); /* marcador final de registro */

	fclose(fsaida);

	fflush(stdin);
	getchar();

	return 0;
}

