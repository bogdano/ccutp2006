#include <unistd.h>
#include <stdio.h>
#include <sys/io.h>

bin(unsigned char value, char *repr) {
	int i = 0;
	for (i = 0; i < 8; i++)
		repr[i] = (value & (1 << i)) ? '1' : '0';
	repr[i] = 0;
}

int main()
{
	unsigned char readed, prev;
	char repr[9];

	if (ioperm(0x378, 3, 1) < 0){
		perror("failed to obtain parallel port access");
		return 1;
	}

	outb(0x23, 0x37A);
	outb(0xFF, 0x378);
	while (1) {
		readed = ~inb(0x378);
		bin(readed, repr);

		if (readed != prev)
			printf("changed to: %s\n", repr);
		printf("\rreaded: %s          ", repr);
		prev = readed;
		usleep(100);
	}
}
