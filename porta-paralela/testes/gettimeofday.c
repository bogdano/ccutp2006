#include <stdio.h>
#include <sys/time.h>

int main()
{
	struct timeval tv;

	if(gettimeofday(&tv, NULL) < 0) {
		perror("failed to gettimeofday");
		return 1;
	}

	printf("seconds: %ld, useconds: %ld\n", tv.tv_sec, tv.tv_usec);

	return 0;
}
