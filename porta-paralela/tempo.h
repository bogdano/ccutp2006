#ifndef inc_tempo_h
#define inc_tempo_h

double gettimedelta(struct timeval*, struct timeval*);

#ifdef WIN32
#include "tempo_win32.h"
#else
#include "tempo_posix.h"
#endif

#endif
