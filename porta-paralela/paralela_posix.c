#include <stdio.h>

#include "paralela_posix.h"

int inicializa_leitor_paralela(void)
{
	if (ioperm(0x378, 3, 1) < 0) {
		perror("nao consegui a porta paralela (esta como root?)");
		return -1;
	}

	return 0;
}
